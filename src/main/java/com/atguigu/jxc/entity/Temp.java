package com.atguigu.jxc.entity;

import lombok.Data;

/**
 * @author terminal
 * @since 2023-07-05 3:10:33
 */
@Data
public class Temp {

    private Goods goods;
    private GoodsType goodsType;
}
