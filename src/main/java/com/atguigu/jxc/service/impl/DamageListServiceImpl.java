package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.jxc.dao.DamageListMapper;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author terminal
 * @since 2023-07-05 13:30:22
 */
@Service
@Slf4j
public class DamageListServiceImpl implements DamageListService {
    @Resource
    private DamageListMapper damageListMapper;

    /**
     * 保存报损单
     *
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @Override
    public void saveDamage(HttpSession session, DamageList damageList, String damageListGoodsStr) {

        log.info("damageListGoodsStr = {}" + damageListGoodsStr);

        User user = (User) session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        damageList.setUserId(userId);
        damageListMapper.saveDamage1(damageList);
        Integer damageListId = damageList.getDamageListId();
        TypeReference<List<DamageListGoods>> typeReference = new TypeReference<List<DamageListGoods>>() {
        };

        List<DamageListGoods> damageListGoodsList = JSON.parseObject(damageListGoodsStr, typeReference);

        damageListGoodsList.stream().forEach(damageListGoods ->{
            damageListGoods.setDamageListId(damageListId);
            damageListMapper.saveDamage2(damageListGoods);
            System.out.println("------------------------------------");
            System.out.println("damageListGoods.getDamageListId() = " + damageListGoods.getDamageListId());
            System.out.println("------------------------------------");
        });

    }

    /**
     * 报损单查询
     *
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        HashMap<String, Object> mapResult = new HashMap<>();
        List<DamageList> list = damageListMapper.list(sTime, eTime);
        mapResult.put("rows", list);
        return mapResult;
    }

    /**
     * 查询报损单商品信息
     *
     * @param damageListId
     * @return
     */
    @Override
    public Map<String, Object> goodsList(Integer damageListId) {
        HashMap<String, Object> mapResult = new HashMap<>();
        List<DamageListGoods> list = damageListMapper.goodsList(damageListId);
        mapResult.put("rows", list);
        return mapResult;
    }
}
