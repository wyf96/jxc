package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierMapper;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author terminal
 * @since 2023-07-04 14:09:33
 */
@Service
@Slf4j
public class SupplierServiceImpl implements SupplierService{
    @Resource
    private SupplierMapper supplierMapper;
    /**
     * 分页查询供应商
     * @return
     */
    @Override
    public Map<String, Object> supplierPage(Integer page, Integer rows, String supplierName) {
        HashMap<String, Object> mapResult = new HashMap<>();

        page = page == 0 ? 1 : page;
        Integer offSet = (page - 1) * rows;

        List<Supplier> supplierList = supplierMapper.pageList(offSet,rows,supplierName);
        mapResult.put("total",supplierMapper.getTotal());
        mapResult.put("rows",supplierList);
        return mapResult;
    }

    /**
     * 添加/修改供应商
     * @param supplier
     * @return
     */
    @Override
    public void saveSupplier(Supplier supplier) {
        if (supplier.getSupplierId() == null){
            //添加
            supplierMapper.insertSupplier(supplier);
        } else {
            //修改
            supplierMapper.updateSupplier(supplier);
        }
    }

    /**
     * 批量删除
     * @param idsList
     */
    @Override
    public void deleteByIds(String[] idsList) {
        for (String id : idsList) {
            supplierMapper.deleteIds(id);
        }
    }
}
