package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.jxc.dao.OverflowListMapper;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.OverflowListService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author terminal
 * @since 2023-07-05 15:16:56
 */
@Service
@Slf4j
public class OverflowListImpl implements OverflowListService {
    @Resource
    private OverflowListMapper overflowListMapper;
    /**
     * 新增报溢单
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    @Override
    public void saveOverflow(HttpSession session,OverflowList overflowList, String overflowListGoodsStr) {
        User user = (User)session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        overflowList.setUserId(userId);
        overflowListMapper.saveOverflow1(overflowList);
        Integer overflowListId = overflowList.getOverflowListId();

        TypeReference<List<OverflowListGoods>> typeReference = new TypeReference<List<OverflowListGoods>>() {};
        List<OverflowListGoods> overflowListGoods = JSON.parseObject(overflowListGoodsStr, typeReference);
        for (OverflowListGoods overflowListGood : overflowListGoods) {
            overflowListGood.setOverflowListId(overflowListId);
            overflowListMapper.saveOverflow2(overflowListGood);
        }
    }

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        HashMap<String, Object> mapResult = new HashMap<>();

        List<OverflowList> list= overflowListMapper.list(sTime,eTime);
        mapResult.put("rows",list);
        return mapResult;
    }

    /**
     * 查询报损单商品信息
     * @param overflowListId
     * @return
     */
    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {
        HashMap<String, Object> mapResult = new HashMap<>();
        List<OverflowListGoods> list= overflowListMapper.goodsList(overflowListId);
        mapResult.put("rows",list);
        return mapResult;
    }
}
