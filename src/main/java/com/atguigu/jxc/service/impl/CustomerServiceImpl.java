package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerMapper;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author terminal
 * @since 2023-07-04 17:52:26
 */
@Service
public class CustomerServiceImpl implements CustomerService {
    @Resource
    private CustomerMapper customerMapper;
    /**
     * 客户列表分页加模糊查询
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @Override
    public Map<String, Object> customerPageList(Integer page, Integer rows, String customerName) {
        HashMap<String, Object> mapResult = new HashMap<>();

        page = page == 0 ? 1 : page;
        Integer offSet = (page - 1) * rows;
        List<Customer> customerList = customerMapper.pageList(offSet,rows,customerName);

        mapResult.put("total",customerMapper.getTotal());
        mapResult.put("rows",customerList);
        return mapResult;
    }

    /**
     * 添加/修改客户
     * @param customer
     * @return
     */
    @Override
    public void saveCustomer(Customer customer) {
        if (customer.getCustomerId() == null){
            // 添加
            customerMapper.save(customer);
        } else {
            // 修改
            customerMapper.update(customer);
        }
    }

    /**
     * 删除客户--批量删除
     * @param ids
     */
    @Override
    public void deleteCustomer(String[] ids) {
        for (String id : ids) {
            customerMapper.delete(id);
        }
    }
}
