package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.UnitMapper;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.UnitService;
import org.springframework.stereotype.Service;
import sun.management.counter.Units;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author terminal
 * @since 2023-07-04 21:28:35
 */
@Service
public class UnitServiceImpl implements UnitService {
    @Resource
    private UnitMapper unitMapper;
    /**
     * 查询所有商品单位
     * @return
     */
    @Override
    public Map<String, Object> list() {
        HashMap<String, Object> mapResult = new HashMap<>();
        List<Unit> unitsList = unitMapper.list();
        mapResult.put("rows",unitsList);
        return mapResult;

    }
}
