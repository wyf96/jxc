package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.TempMapper;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
@Slf4j
public class GoodsServiceImpl implements GoodsService {

    @Resource
    private GoodsDao goodsDao;
    @Resource
    private TempMapper tempMapper;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    /**
     * 分页查询商品库存信息+模糊查询
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        HashMap<String, Object> mapResult = new HashMap<>();

        page = page == 0 ? 1 : page;
        Integer offSet = (page - 1) * rows;

        List<Goods> goodsList = goodsDao.pageList(offSet, rows, codeOrName, goodsTypeId);

        mapResult.put("total", goodsDao.getTotal());
        mapResult.put("rows", goodsList);
        return mapResult;
    }

    /**
     * 分页查询商品信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param goodsName   商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @Override
    public Map<String, Object> goodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        HashMap<String, Object> mapResult = new HashMap<>();

        page = page == 0 ? 1 : page;
        Integer offSet = (page - 1) * rows;

        List<Goods> goodsList = null;

        if (goodsTypeId != null && goodsTypeId == 1) {
            goodsList = goodsDao.selectAll(offSet, rows, goodsName, goodsTypeId);
        } else {
            goodsList = goodsDao.goodList(offSet, rows, goodsName, goodsTypeId);

        }

        mapResult.put("total", goodsDao.getTotal());
        mapResult.put("rows", goodsList);
        return mapResult;
    }

    /**
     * 添加或修改商品信息
     *
     * @param goods 商品信息实体
     * @return
     */
    @Override
    public void saveGoods(Goods goods) {
        if (goods.getGoodsId() == null) {
            //添加
            goods.setInventoryQuantity(0);
            goods.setLastPurchasingPrice(0.0);
            goods.setState(0);
            goodsDao.save(goods);
        } else {
            //修改
            goods.setInventoryQuantity(0);
            goods.setLastPurchasingPrice(0.0);
            goods.setState(0);
            goodsDao.update(goods);
        }
    }

    /**
     * 删除商品信息
     *
     * @param goodsId 商品ID
     * @return
     */
    @Override
    public void deleteGoods(Integer goodsId) {
        Goods goods = goodsDao.selectById(goodsId);
        if (goods.getState() != 0) {
            //return new ServiceVO<>(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
            throw new RuntimeException("不可删除");
        }

        goodsDao.deleteById(goodsId);
    }

    /**
     * 分页查询无库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        HashMap<String, Object> mapResult = new HashMap<>();

        page = page == 0 ? 1 : page;
        Integer offSet = (page - 1) * rows;

        List<Goods> goodsList = goodsDao.getNoInventoryQuantity(offSet, rows, nameOrCode);

        mapResult.put("total", goodsDao.getTotal());
        mapResult.put("rows", goodsList);
        return mapResult;
    }

    /**
     * 分页查询有库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        HashMap<String, Object> mapResult = new HashMap<>();

        page = page == 0 ? 1 : page;
        Integer offSet = (page - 1) * rows;

        List<Goods> goodsList = goodsDao.getHasInventoryQuantity(offSet, rows, nameOrCode);

        mapResult.put("total", goodsDao.getTotal());
        mapResult.put("rows", goodsList);
        return mapResult;
    }

    /**
     * 添加商品期初库存
     *
     * @param goodsId           商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice   成本价
     * @return
     */
    @Override
    public void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        goodsDao.saveStock(goodsId, inventoryQuantity, purchasingPrice);
    }

    /**
     * 删除商品库存
     *
     * @param goodsId 商品ID
     * @return
     */
    @Override
    public void deleteStock(Integer goodsId) {
        Goods goods = goodsDao.selectById(goodsId);
        if (goods.getState() != 0) {
            throw new RuntimeException("状态不可删除");
        }
        goodsDao.deleteById(goodsId);
    }

    /**
     * 查询库存报警商品信息
     * @return
     */
    @Override
    public Map<String, Object> listAlarm() {
        HashMap<String, Object> mapResult = new HashMap<>();
        List<Goods> goodsList = goodsDao.listAlarm();
        mapResult.put("rows",goodsList);
        return mapResult;
    }
}
