package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowList;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author terminal
 * @since 2023-07-05 15:16:50
 */
public interface OverflowListService {
    /**
     * 新增报溢单
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    void saveOverflow(HttpSession session,OverflowList overflowList, String overflowListGoodsStr);

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    Map<String, Object> list(String sTime, String eTime);

    /**
     * 查询报损单商品信息
     * @param overflowListId
     * @return
     */
    Map<String, Object> goodsList(Integer overflowListId);
}
