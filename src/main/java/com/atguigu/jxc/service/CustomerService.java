package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.Map;

/**
 * @author terminal
 * @since 2023-07-04 17:52:01
 */
public interface CustomerService {
    /**
     * 客户列表分页加模糊查询
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    Map<String, Object> customerPageList(Integer page, Integer rows, String customerName);

    /**
     * 添加/修改 客户
     * @param customer
     * @return
     */
    void saveCustomer(Customer customer);

    /**
     * 删除客户
     * @param ids
     * @return
     */
    void deleteCustomer(String[] ids);
}
