package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.List;
import java.util.Map;

/**
 * @author terminal
 * @since 2023-07-04 14:09:22
 */
public interface SupplierService {
    /**
     * 分页查询供应商
     * @return
     */
    Map<String, Object> supplierPage(Integer page, Integer rows, String supplierName);

    /**
     * 添加/修改供应商
     * @param supplier
     * @return
     */
    void saveSupplier(Supplier supplier);




    /**
     * 批量删除
     * @param idsList
     */
    void deleteByIds(String[] idsList);

}
