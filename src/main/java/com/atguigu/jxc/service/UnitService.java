package com.atguigu.jxc.service;

import java.util.Map;

/**
 * @author terminal
 * @since 2023-07-04 21:28:28
 */
public interface UnitService {
    /**
     * 查询所有商品单位
     * @return
     */
    Map<String, Object> list();
}
