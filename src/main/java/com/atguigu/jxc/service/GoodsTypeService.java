package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;

import java.util.ArrayList;

/**
 * @description
 */
public interface GoodsTypeService {
    ArrayList<Object> loadGoodsType();

    /**
     * 添加分类
     */
    void save(String goodsTypeName,Integer pId);

    /**
     * 删除指定分类--ID
     */
    void delete(Integer goodsTypeId);
}
