package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author terminal
 * @since 2023-07-05 13:30:07
 */
public interface DamageListService {
    /**
     * 保存报损单
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    void saveDamage(HttpSession session,DamageList damageList, String damageListGoodsStr);

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    Map<String, Object> list(String sTime, String eTime);

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    Map<String, Object> goodsList(Integer damageListId);
}
