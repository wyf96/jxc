package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverflowListService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author terminal
 * @since 2023-07-05 15:15:27
 */
@RestController
@RequestMapping("overflowListGoods")
public class OverflowListController {
    @Resource
    private OverflowListService overflowListService;

    /**
     * 新增报溢单
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    @PostMapping("/save")
    public ServiceVO saveOverflow(HttpSession session,OverflowList overflowList, String overflowListGoodsStr){
        overflowListService.saveOverflow(session,overflowList,overflowListGoodsStr);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("list")
    public Map<String, Object> list(String  sTime, String  eTime){
        return overflowListService.list(sTime,eTime);
    }

    /**
     * 查询报损单商品信息
     * @param overflowListId
     * @return
     */
    @PostMapping("/goodsList")
    public Map<String, Object> goodsList(Integer overflowListId){
        return overflowListService.goodsList(overflowListId);
    }
}
