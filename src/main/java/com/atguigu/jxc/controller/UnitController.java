package com.atguigu.jxc.controller;

import com.atguigu.jxc.service.UnitService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author terminal
 * @since 2023-07-04 21:27:35
 */
@RestController
@RequestMapping("/unit")
public class UnitController {

    @Resource
    private UnitService unitService;

    /**
     * 查询所有商品单位
     * @return
     */
    @PostMapping("/list")
    private Map<String, Object> unitList() {
        return unitService.list();
    }
}
