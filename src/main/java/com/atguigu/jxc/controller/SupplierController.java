package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author terminal
 * @since 2023-07-04 14:02:41
 */
@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Resource
    private SupplierService supplierService;

    /**
     * 分页查询供应商
     *
     * @return
     */
    @PostMapping("/list")
    public Map<String, Object> supplierList(Integer page, Integer rows, String supplierName) {
        return supplierService.supplierPage(page, rows, supplierName);
    }

    /**
     * 添加/修改供应商
     * @param supplier
     * @return
     */
    @PostMapping("/save")
    public ServiceVO saveSupplier(Supplier supplier) {
        supplierService.saveSupplier(supplier);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO deleteSupplier(String ids) {

        String[] idsList = ids.split(",");
        supplierService.deleteByIds(idsList);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }
}
