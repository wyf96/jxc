package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author terminal
 * @since 2023-07-04 17:50:50
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Resource
    private CustomerService customerService;

    /**
     * 客户列表分页加模糊查询
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @PostMapping("/list")
    public Map<String, Object> customerPageList(Integer page, Integer rows, String customerName){
        return customerService.customerPageList(page,rows,customerName);
    }

    /**
     * 添加/修改 客户
     * @param customer
     * @return
     */
    @PostMapping("/save")
    public ServiceVO saveCustomer(Customer customer){
        customerService.saveCustomer(customer);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除客户
     * @param ids
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO deleteCustomer(String ids){
        String[] idArr = ids.split(",");
        customerService.deleteCustomer(idArr);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }
}
