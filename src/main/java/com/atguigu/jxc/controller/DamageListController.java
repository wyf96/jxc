package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageListService;
import com.google.gson.JsonObject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author terminal
 * @since 2023-07-05 13:29:35
 */
@RestController
@RequestMapping("damageListGoods")
public class DamageListController {
    @Resource
    private DamageListService damageListService;

    /**
     * 保存报损单
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @PostMapping("/save")
    public ServiceVO saveDamage(HttpSession session,DamageList damageList, String damageListGoodsStr){
        damageListService.saveDamage(session,damageList,damageListGoodsStr);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("list")
    public Map<String, Object> list(String  sTime, String  eTime){
        return damageListService.list(sTime,eTime);
    }

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    @PostMapping("/goodsList")
    public Map<String, Object> goodsList(Integer damageListId){
        return damageListService.goodsList(damageListId);
    }
}
