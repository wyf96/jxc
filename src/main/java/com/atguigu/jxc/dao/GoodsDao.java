package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();

    /**
     * 分页查询商品库存信息+模糊查询
     *
     * @param offSet      当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    List<Goods> pageList(@Param("offSet") Integer offSet, @Param("rows") Integer rows,
                         @Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    /**
     * 获得总页数
     *
     * @return
     */
    Integer getTotal();

    List<Goods> goodList(@Param("offSet") Integer offSet, @Param("rows") Integer rows,
                         @Param("goodsName") String goodsName, @Param("goodsTypeId") Integer goodsTypeId);


    /**
     * 查询所有
     * @param offSet
     * @param rows
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    List<Goods> selectAll(@Param("offSet") Integer offSet, @Param("rows") Integer rows,
                          @Param("goodsName") String goodsName, @Param("goodsTypeId") Integer goodsTypeId);

    /**
     * 添加
      * @param goods
     */
    void save( Goods goods);

    /**
     * 修改
     * @param goods
     */
    void update( Goods goods);

    /**
     * 根据Id查对象
     * @param goodsId
     * @return
     */
    Goods selectById(@Param("goodsId") Integer goodsId);

    /**
     * 根据ID删除对象
     * @param goodsId
     */
    void deleteById(@Param("goodsId") Integer goodsId);

    /**
     * 无库存商品列表展示
     * @param offSet
     * @param rows
     * @param nameOrCode
     * @return
     */
    List<Goods> getNoInventoryQuantity(@Param("offSet") Integer offSet, @Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);


    List<Goods> getHasInventoryQuantity(@Param("offSet") Integer offSet, @Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);


    void saveStock(@Param("goodsId") Integer goodsId, @Param("inventoryQuantity") Integer inventoryQuantity, @Param("purchasingPrice") double purchasingPrice);



    List<Goods> listAlarm();
}
