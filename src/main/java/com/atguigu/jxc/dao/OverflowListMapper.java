package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author terminal
 * @since 2023-07-05 15:16:28
 */
public interface OverflowListMapper {
    void saveOverflow1(OverflowList overflowList);

    void saveOverflow2(OverflowListGoods overflowListGood);

    List<OverflowList> list(@Param("sTime") String sTime, @Param("eTime") String eTime);

    List<OverflowListGoods> goodsList(@Param("overflowListId") Integer overflowListId);
}
