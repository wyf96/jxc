package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author terminal
 * @since 2023-07-04 18:05:46
 */
public interface CustomerMapper {
    /**
     *
     * @param offSet
     * @param rows
     * @param customerName
     * @return
     */
    List<Customer> pageList(@Param("offSet") Integer offSet, @Param("rows")Integer rows, @Param("customerName")String customerName);

    /**
     *
     * @return
     */
    Integer getTotal();

    /**
     * 添加
     * @param customer
     */
    void save(Customer customer);

    /**
     * 修改
     * @param customer
     */
    void update(Customer customer);

    /**
     * 删除
     * @param id
     */
    void delete(String id);
}
