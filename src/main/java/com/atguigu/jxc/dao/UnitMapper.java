package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;
import sun.management.counter.Units;

import java.util.List;

/**
 * @author terminal
 * @since 2023-07-04 21:28:17
 */
public interface UnitMapper {

    /**
     * 查询所有商品单位
     */
    List<Unit> list();
}
