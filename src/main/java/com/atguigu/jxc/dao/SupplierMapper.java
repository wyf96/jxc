package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author terminal
 * @since 2023-07-04 14:32:06
 */
public interface SupplierMapper {
    /**
     * 分页查询供应商
     * @return
     */
    List<Supplier> pageList(@Param("offSet") Integer offSet, @Param("rows") Integer rows, @Param("supplierName") String supplierName);

    /**
     * 获取总页数
     * @return
     */
    Integer getTotal();

    /**
     * 添加供应商
     * @param supplier
     * @return
     */
    void insertSupplier(Supplier supplier);

    /**
     * 修改供应商
     * @param supplier
     * @return
     */
    void updateSupplier(Supplier supplier);

    /**
     * 批量删除
     * @param id
     */
    void deleteIds(String id);

}
