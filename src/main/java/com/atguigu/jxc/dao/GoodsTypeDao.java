package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);

    /**
     * 添加分类
     */
    void save(@Param("goodsTypeName") String goodsTypeName, @Param("pId") Integer pId, @Param("STATE") Integer STATE);

    /**
     * 删除指定分类--ID
     */
    void delete(@Param("goodsTypeId") Integer goodsTypeId);
}
