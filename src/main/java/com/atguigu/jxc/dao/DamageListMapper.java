package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author terminal
 * @since 2023-07-05 13:29:55
 */
public interface DamageListMapper {


    void saveDamage1(DamageList damageList);

    void saveDamage2(DamageListGoods damageListGoods);


    List<DamageList> list(@Param("sTime") String sTime, @Param("eTime") String eTime);

    List<DamageListGoods> goodsList(@Param("damageListId") Integer damageListId);

    void saveId(@Param("id") Integer id);
}
