package com.atguigu.jxc;

import lombok.Data;

/**
 * @author terminal
 * @since 2023-07-07 8:36:19
 */
@Data
public class StudentTest {
    private Integer stuId;
    private String stuName;
    private Integer stuAge;
}
