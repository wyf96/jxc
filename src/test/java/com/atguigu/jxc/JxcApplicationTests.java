package com.atguigu.jxc;


import com.alibaba.fastjson.JSONObject;
import com.atguigu.jxc.entity.GoodsType;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@SpringBootTest
@Slf4j
public class JxcApplicationTests {

	public static void main(String[] args) {
		String startTimeStr = "2023-07-01";
		String endTimeStr = "2023-07-15";

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		try {
			Date startTime = sdf.parse(startTimeStr);
			Date endTime = sdf.parse(endTimeStr);
			Date currentTime = new Date();

			long currentMillis = currentTime.getTime();
			long startMillis = startTime.getTime();
			long endMillis = endTime.getTime();

			if (currentMillis >= startMillis && currentMillis <= endMillis) {
				System.out.println("当前时间在开始时间和结束时间之间");
			} else {
				System.out.println("当前时间不在开始时间和结束时间之间");
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void contextLoads() {
		String str = "{\n" +
				"  \"stuId\": 40,\n" +
				"  \"stuName\": \"test_cf761af54e55\",\n" +
				"  \"stuAge\": 11\n" +
				"}"

		;

		log.error(str);
		Class<StudentTest> List = null;
		StudentTest studentTest = JSONObject.parseObject(str, List);
		System.out.println("studentTest.getStuId() = " + studentTest.getStuId());
		System.out.println("studentTest.getStuName() = " + studentTest.getStuName());
		System.out.println("studentTest.getStuAge() = " + studentTest.getStuAge());
	}

}
